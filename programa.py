for i in range(1, 10):
    numero = i
    resultado = 1 

    while numero > 1:
        resultado *= numero
        numero -= 1

    print(f"{i}! = {resultado}")

