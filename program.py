def calcular_factorial(numero):
    if numero == 0 or numero == 1:
        return 1
    else:
        return numero * calcular_factorial(numero - 1)

for i in range(1, 11):
    factorial_resultado = calcular_factorial(i)
    print(f"El factorial de {i} es: {factorial_resultado}")